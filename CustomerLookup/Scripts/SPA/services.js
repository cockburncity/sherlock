﻿

    app.service('customerLookupService', function ($http) {
        //return $resource('/api/CustomerLookup/Phone/:id', { id: '@_id' });

        

        /************
        SYSTEMS
        *************/

        //P&R people
        this.getPropertyAndRatingPeople = function (formData) {
            console.log("getPropertyAndRatingPeople formData", formData);
            return this.doCustomerLookupRequest("PropertyAndRatingCustomer", formData);
            //return $http.get(appname + "/api/CustomerLookup/PropertyAndRatingCustomer/" + phoneNumber);
        }

        //F1 creditors
        this.getFinanceOneBusiness = function (formData) {
            return this.doCustomerLookupRequest("FinanceOne", formData);
            //return $http.get(appname + "/api/CustomerLookup/Phone/FinanceOne/" + phoneNumber);
        }

        //Amlib people
        this.getAmlibPeople = function (formData) {
            return this.doCustomerLookupRequest("Amlib", formData);
            //return $http.get(appname + "/api/CustomerLookup/Phone/Amlib/" + phoneNumber);
        }

        //IntelliLeisure
        this.getIntelliLeisurePeople = function (formData) {
            return this.doCustomerLookupRequest("IntelliLeisure", formData);
            //return $http.get(appname + "/api/CustomerLookup/Phone/ClassLeisureWorld/" + phoneNumber);
        }

        //Class LW
        this.getClassLeisureWorldPeople = function (formData) {
            return this.doCustomerLookupRequest("ClassLeisureWorld", formData);
            //return $http.get(appname + "/api/CustomerLookup/Phone/ClassLeisureWorld/" + phoneNumber);
        }

        //Class facility bookings
        this.getClassAdministrationPeople = function (formData) {
            return this.doCustomerLookupRequest("ClassAdministration", formData);
           // return $http.get(appname + "/api/CustomerLookup/Phone/ClassFacilityBooking/" + phoneNumber);
        }

        this.doCustomerLookupRequest = function (customer, formData) {
            if (typeof formData == 'undefined') {
                console.log("doCustomerLookupRequest formData is undefined");
                return false;
            }
            var querystring = "";
            if (formData.number) {
                querystring += "number=" + formData.number + "&";
            }
            if (formData.givennames) {
                querystring += "givennames=" + formData.givennames + "&";
            }
            if (formData.surname) {
                querystring += "surname=" + formData.surname + "&";
            }
            if (formData.address) {
                querystring += "address=" + formData.address + "&";
            }
            if (formData.email) {
                querystring += "email=" + formData.email + "&";
            }
            if (querystring.length > 5) {
                querystring = querystring.substring(0, querystring.length - 1);
                console.log("query string = " + querystring);
                return $http.get(appname + "/api/CustomerLookup/" + customer + "/?" + querystring);
            } else {
                console.log("No query string");
                return false;
            }
        }

        /************
        ASSOCIATIONS
        *************/

        //Animals
        this.getAssociatedAnimals = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Animals/" + status + "/" + associationId);
        }

        //Applications
        this.getAssociatedApplications = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Applications/" + status + "/" + associationId);
        }

        //Infringements
        this.getAssociatedInfringements = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Infringements/" + status + "/" + associationId);
        }

        //Properties
        this.getAssociatedProperties = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Properties/" + status + "/" + associationId);
        }

        //Requests
        this.getAssociatedRequests = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Requests/" + status + "/" + associationId);
        }

        //Debtors
        this.getAssociatedDebtors = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Debtors/" + status + "/" + associationId);
        }

        //Loans
        this.getAssociatedLoans = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/Loans/" + status + "/" + associationId);
        }

        //LW Memberships
        this.getAssociatedMemberships = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldMemberships/" + status + "/" + associationId);
        }

        //Bookings
        this.getAssociatedBookings = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldBookings/" + status + "/" + associationId);
        }

        //Classes
        this.getAssociatedClasses = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldClasses/" + status + "/" + associationId);
        }

        //Courses
        this.getAssociatedCourses = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldCourses/" + status + "/" + associationId);
        }
        
        //Outstanding invoices
        this.getAssociatedInvoices = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldInvoices/" + status + "/" + associationId);
        }

        //Groups
        this.getAssociatedGroups= function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldGroups/" + status + "/" + associationId);
        }


        //Accounts
        this.getAssociatedAccounts = function (associationId, status) {
            return $http.get(appname + "/api/CustomerLookup/Associated/LeisureWorldAccounts/" + status + "/" + associationId);
        }

    });
