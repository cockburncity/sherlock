﻿app.controller('formController', function ($scope, customerLookupService) {
    
    console.log("formData in formcontroller is ", $scope.formData);

    $scope.CallingNumber = PhoneNumber;

    $scope.doSearch = function (scope) {
        console.log("DO SEARCH", $scope.formData);
        $scope.formData.errorMessage = "";
        var performSearch = true;
        if ($scope.advSearch.surname.$invalid) {
            performSearch = false;
            console.log("$scope.advSearch.surname.$invalid");
            $scope.formData.errorMessage += "Please enter a valid " + ($scope.formData.searchbusiness ? "business name" : "surname") + ". ";
        }
        if ($scope.advSearch.givennames.$invalid) {
            performSearch = false;
            console.log("$scope.advSearch.givennames.$invalid");
            $scope.formData.errorMessage += "Please enter a valid first name and/or middle names. "
        }
        if ($scope.advSearch.address.$invalid) {
            performSearch = false;
            console.log("$scope.advSearch.address.$invalid");
            $scope.formData.errorMessage += "Please enter a longer address to search with. "
        }
        if ($scope.advSearch.number.$invalid) {
            performSearch = false;
            console.log("$scope.advSearch.number.$invalid");
            $scope.formData.errorMessage += "Please enter a valid phone number. "
        }
        if ($scope.advSearch.email.$invalid) {
            performSearch = false;
            console.log("$scope.advSearch.email.$invalid");
            $scope.formData.errorMessage += "Please enter a valid email address "
        }
        if (performSearch) {
            $scope.$broadcast('doSearch');
            $scope.showsearch = false;
        } else {
            $scope.showsearch = true;
        }
        /*findPropertyRatingPeople($scope.formData);
        findPropertyRatingBusiness($scope.formData);
        findFinanceOneBusiness($scope.formData);
        findAmlibPeople($scope.formData);
        findClassLeisureWorldPeople($scope.formData);
        findClassAdministrationPeople($scope.formData);*/
    }


}).controller('propertyRatingPersonController', function ($scope, customerLookupService) {

    $scope.listLimit = 10;
    findPropertyRatingPeople($scope.formData);
    console.log("$scope.formData", $scope.formData);
    $scope.$on('doSearch', function (e) {
        findPropertyRatingPeople($scope.formData);
    });

    //Search for people in P&R
    function findPropertyRatingPeople(formData) {


        console.log("findPropertyRatingPeople formData", formData);
        $scope.CallingNumber = PhoneNumber;

        var promiseGet = customerLookupService.getPropertyAndRatingPeople(formData);

        promiseGet.then(function (pl) { $scope.PropertyRatingPeople = pl.data },
              function (errorPl) {
                  $log.error('failure loading property and rating people', errorPl);
              });
    }

    //associated animals
    $scope.getAnimals = function (viewScope, associationId, status) {

        //alert("Get animals called with status " + status);
        var promiseGet = customerLookupService.getAssociatedAnimals(associationId, status);

        promiseGet.then(function (pl) {
            /*viewScope.AssociatedAnimals = pl.data;
            console.log("Returned animals", pl.data);

                if (!$scope.$$phase)
                    $scope.$apply();
            */
                //setTimeout(function () {
                    //viewScope.$apply(function () {
                        console.log("Returned animals", pl.data);
                        viewScope.AssociatedAnimals = pl.data;
                   // });
               // }, 2000);
            },
              function (errorPl) {
                  $log.error('failure loading associated animals', errorPl);
              });
    }

    //associated applications
    $scope.getApplications = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedApplications(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedApplications = pl.data },
              function (errorPl) {
                  $log.error('failure loading associated applications', errorPl);
              });
    }

    //associated infringements
    $scope.getInfringements = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedInfringements(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedInfringements = pl.data },
              function (errorPl) {
                  $log.error('failure loading associated Infringements', errorPl);
              });
    }

    //associated properties
    $scope.getProperties = function (viewScope, associationId, status) {
        console.log("Load " + status + " properties");



        var promiseGet = customerLookupService.getAssociatedProperties(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedProperties = pl.data },
              function (errorPl) {
                  $log.error('failure loading associated properties', errorPl);
              });
    }

    //associated requests
    $scope.getRequests = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedRequests(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedRequests = pl.data },
              function (errorPl) {
                  $log.error('failure loading associated Requests', errorPl);
              });
    }

    //associated requests
    $scope.getDebtors = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedDebtors(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedDebtors = pl.data },
              function (errorPl) {
                  $log.error('failure loading associated debtors', errorPl);
              });
    } 


})
.controller('propertyRatingBusinessController', function ($scope, customerLookupService) {

    findPropertyRatingBusiness($scope.formData);
    $scope.$on('doSearch', function (e) {
        findPropertyRatingBusiness($scope.formData);
    });

    //search for businesses in P&R
    function findPropertyRatingBusiness(formData) {
        
        $scope.CallingNumber = PhoneNumber;

        var promiseGet = customerLookupService.getPropertyAndRatingBusiness(formData);

        promiseGet.then(function (pl) { $scope.PropertyRatingBusiness= pl.data },
              function (errorPl) {
                  $log.error('failure loading property and rating people', errorPl);
              });
    }

    $scope.getProperties = function (NucNameId) {
        alert(NucNameId);
    }

})
.controller('financeOneController', function ($scope, customerLookupService) {
    findFinanceOneBusiness($scope.formData);

    $scope.$on('doSearch', function (e) {
        findFinanceOneBusiness($scope.formData);
    });

    //Search for creditors in F1
    function findFinanceOneBusiness(formData) {

        $scope.CallingNumber = PhoneNumber;

        var promiseGet = customerLookupService.getFinanceOneBusiness(formData);

        promiseGet.then(function (pl) { $scope.FinanceOneBusiness = pl.data },
              function (errorPl) {
                  $log.error('failure loading property and rating people', errorPl);
              });
    }

    $scope.getProperties = function (NucNameId) {
        alert(NucNameId);
    }

})
.controller('intelliLeisureController', function ($scope, customerLookupService) {

    $scope.listLimit = 10;

    findIntelliLeisurePeople($scope.formData);

    $scope.$on('doSearch', function (e) {
        findIntelliLeisurePeople($scope.formData);
    });

    //Search for people in IntelliLeisure
    function findIntelliLeisurePeople(formData) {

        $scope.CallingNumber = PhoneNumber;

        var promiseGet = customerLookupService.getIntelliLeisurePeople(formData);

        promiseGet.then(function (pl) { $scope.IntelliLeisurePeople = pl.data },
                function (errorPl) {
                    $log.error('failure loading intellileisure people', errorPl);
                });
    }

    //associated requests
    $scope.getLeisureWorldMemberships = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedMemberships(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned memberships", pl.data);
            viewScope.AssociatedMemberships = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated memberships', errorPl);
        });
    }

    $scope.getBookings = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedBookings(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned bookings", pl.data);
            viewScope.AssociatedBookings = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated bookings', errorPl);
        });
    }

    $scope.getClasses = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedClasses(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned classes", pl.data);
            viewScope.AssociatedClasses = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated classes', errorPl);
        });
    }

    $scope.getCourses = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedCourses(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned courses", pl.data);
            viewScope.AssociatedCourses = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated courses', errorPl);
        });
    }

    $scope.getLeisureWorldInvoices = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedInvoices(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned invoices", pl.data);
            viewScope.AssociatedInvoices = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated invoices', errorPl);
        });
    }
    $scope.getLeisureWorldGroups = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedGroups(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned groups", pl.data);
            viewScope.AssociatedGroups = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated  groups', errorPl);
        });
    }

    $scope.getOnlineAccounts = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedAccounts(associationId, status);

        promiseGet.then(function (pl) {
            console.log("Returned online accounts", pl.data);
            viewScope.AssociatedAccounts = pl.data
        },
        function (errorPl) {
            $log.error('failure loading associated  account details', errorPl);
        });
    }

})
.controller('classAdministrationController', function ($scope, customerLookupService) {
    findClassAdministrationPeople($scope.formData);

    $scope.$on('doSearch', function (e) {
        findClassAdministrationPeople($scope.formData);
    });
    
    //Search for people in Class (bookings)
    function findClassAdministrationPeople(formData) {

        $scope.CallingNumber = PhoneNumber;

        var promiseGet = customerLookupService.getClassAdministrationPeople(formData);

        promiseGet.then(function (pl) { $scope.ClassAdministrationPeople = pl.data },
              function (errorPl) {
                  $log.error('failure loading class bookings people', errorPl);
              });
    }

    $scope.getProperties = function(NucNameId) {
        alert(NucNameId);
    }
})
.controller('classLeisureWorldController', function ($scope, customerLookupService) {

    findClassLeisureWorldPeople($scope.formData);

    $scope.$on('doSearch', function (e) {
        findClassLeisureWorldPeople($scope.formData);
    });

        //Search for people in class (LW)
        function findClassLeisureWorldPeople(formData) {

            $scope.CallingNumber = PhoneNumber;

            var promiseGet = customerLookupService.getClassLeisureWorldPeople(formData);

            promiseGet.then(function (pl) { $scope.ClassLeisureWorldPeople = pl.data },
                  function (errorPl) {
                      $log.error('failure loading class leisure world people', errorPl);
                  });
        }

        $scope.getProperties = function (NucNameId) {
            alert(NucNameId);
        }
    })
.controller('amlibController', function ($scope, customerLookupService) {
    findAmlibPeople($scope.formData);

    $scope.$on('doSearch', function (e) {
        findAmlibPeople($scope.formData);
    });

    //Search for people in Amlib
    function findAmlibPeople(formData) {

        $scope.CallingNumber = PhoneNumber;

        var promiseGet = customerLookupService.getAmlibPeople(formData);

        promiseGet.then(function (pl) { $scope.AmlibPeople = pl.data },
              function (errorPl) {
                  $log.error('failure loading amlib people', errorPl);
              });
    }

    //loans
    $scope.getLoans = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedLoans(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedLoans = pl.data;  },
              function (errorPl) {
                  $log.error('failure loading associated loans', errorPl);
              });
    }

    //overdueloans
    $scope.getOverdueLoans = function (viewScope, associationId, status) {

        var promiseGet = customerLookupService.getAssociatedLoans(associationId, status);

        promiseGet.then(function (pl) { viewScope.AssociatedOverdueLoans = pl.data;  },
              function (errorPl) {
                  $log.error('failure loading associated overdue loans', errorPl);
              });
    }

})
.controller('TabController', ['$scope', function ($scope) {
    $scope.tab = 1;

    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };

    $scope.clickToSetStatus = function () {
        $scope.$broadcast('setStatus');
    };

    $scope.parentcurrent = {};
    $scope.isSet = function(tabNum){
        return $scope.tab === tabNum;
    };
}]);

