﻿var app;
var appname = "";
//var appname = "/CustomerLookup";
(function () {
    app = angular.module("customerLookupModule", ['clipBoard'])
    .config(function ($compileProvider) {

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|cilink|ie|iessl|coglaunch|file):/);
        // whitelists non-http: protocols. specifically we need coui for coherent.

    })
    .directive("contactDetails", function () {
        return {
            restrict: 'E',
            templateUrl: appname + '/Scripts/SPA/partials/contact-details.html',
            scope: {
                entity: "="
            },
        };
    })
   .directive("switch", function () {
       return {

           restrict: "AE",
           replace: !0,
           transclude: !0,
           templateUrl: appname + '/Scripts/SPA/partials/switch.html',
           scope: {
               current: "@current",
               callbackfn: '=',
               nucnameid: '@nucnameid',
               viewscope: '=',
               listlength: '=',
               listtype: '@listtype',
               control: '='
           },
           /*controller: function($scope){
               if (angular.isUndefined($scope.current))
                   $scope.current = false;
           },*/
           link: function ($scope, $element, $attrs) {

               $scope.internalControl = $scope.control || {};
               $scope.parentcurrent = $scope.current;
               $scope.internalControl.loadAssociatedData = function () {

                   $scope.viewscope.status = '';
                   if ($scope.current) {
                       status = 'Current';
                   } else {
                       status = 'Past';
                   }
                   //alert("$scope.callbackfn = " + $scope.callbackfn + " and nucnameid = " + $scope.nucnameid + " and status = " + status);

                   $scope.callbackfn($scope.viewscope, $scope.nucnameid, status);

                   //$scope.viewscope.status[0] = "Current";

               }

           },
           controller: function ($scope, $element) {
               $scope.$on('setStatus', function () {
                   console.log("called set status");
                   $scope.current = true;
               });
           }
           /*template: function (n, e) {
               var s = "";
               return s += "<span", s += ' class="switch' + (e.class ? " " + e.class : "") + '"', s += e.ngModel ? ' ng-click="' + e.ngModel + "=!" + e.ngModel + (e.ngChange ? "; " + e.ngChange + '()"' : '"') : "", s += ' ng-class="{ checked:' + e.ngModel + ' }"', s += ">", s += "<small></small>", s += '<input type="checkbox"', s += e.id ? ' id="' + e.id + '"' : "", s += e.name ? ' name="' + e.name + '"' : "", s += e.ngModel ? ' ng-model="' + e.ngModel + '"' : "", s += ' style="display:none" />', s += '<span class="switch-text">', s += e.on ? '<span class="on">Past</span>' : '<span class="on">Past</span>', s += e.off ? '<span class="off">Current</span>' : '<span class="off">Current</span>', s += "</span>"
           }*/
       }
   }).directive('EnterKeyPress', function () {
       return function (scope, element, attrs) {
           element.bind("keydown keypress", function (event) {
               if(event.which === 13) {
                   scope.$apply(function (){
                       $scope.$broadcast('doSearch');
                   });

                   event.preventDefault();
               }
           });
       };
   });;


})();