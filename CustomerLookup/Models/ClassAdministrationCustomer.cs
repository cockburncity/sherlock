﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class ClassAdministrationCustomer : Customer
    {
        private string _personId { get; set; }
        private int _age { get; set; }
        private decimal _balance { get; set; }

        public string PersonId
        {
            get { return _personId; }
            set { _personId = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }
    }
}