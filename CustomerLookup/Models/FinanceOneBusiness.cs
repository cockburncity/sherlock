﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class FinanceOneBusiness : Customer
    {

        private string _ABN { get; set; }
        private string _accountNumber { get; set; }

        public string ABN
        {
            get { return _ABN; }
            set { _ABN = value; }
        }

        public string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }
    }
}