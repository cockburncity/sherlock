﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class AmlibCustomer : Customer
    {
        private string _borrowerId { get; set; }
        private string _borrowerBarcodeNo { get; set; }
        private string _borrowerPin { get; set; }
        private string _borrowerMembershipExpiry { get; set; }
        private decimal _borrowerOwing { get; set; }
        private string _systemCounter { get; set; }

        public string BorrowerId
        {
            get { return _borrowerId; }
            set { _borrowerId = value; }
        }

        public string BorrowerBarcodeNo
        {
            get { return _borrowerBarcodeNo; }
            set { _borrowerBarcodeNo = value; }
        }

        public string BorrowerPin
        {
            get { return _borrowerPin; }
            set { _borrowerPin = value; }
        }

        public string BorrowerMembershipExpiry
        {
            get { return _borrowerMembershipExpiry; }
            set { _borrowerMembershipExpiry = value; }
        }

        public decimal BorrowerOwing
        {
            get { return _borrowerOwing; }
            set { _borrowerOwing = value; }
        }

        public string SystemCounter
        {
            get { return _systemCounter; }
            set { _systemCounter = value; }
        }
    }
}