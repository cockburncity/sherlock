﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class IntelliLeisureCustomer : Customer
    {
        private Guid _userGuid { get; set; }
        private int _branchID { get; set; }
        private string _userID { get; set; }

        public Guid UserGuid
        {
            get { return _userGuid; }
            set { _userGuid = value; }
        }

        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
    }
}