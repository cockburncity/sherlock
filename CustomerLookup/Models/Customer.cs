﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class Customer
    {

        private string _givenNames { get; set; }
        private string _surname { get; set; }
        private string _dateOfBirth { get; set; }
        private string _mobileNumber { get; set; }
        private string _homePhone { get; set; }
        private string _workPhone { get; set; }
        private string _email { get; set; }
        private string _address { get; set; }
        

        public string GivenNames
        {
            get { return _givenNames; }
            set { _givenNames = value; }
        }

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        public string DateOfBirth
        {
            get { return _dateOfBirth; }
            set { _dateOfBirth = value; }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set { _mobileNumber = value; }
        }

        public string HomePhone
        {
            get { return _homePhone; }
            set { _homePhone = value; }
        }

        public string WorkPhone
        {
            get { return _workPhone; }
            set { _workPhone = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }


    }
}