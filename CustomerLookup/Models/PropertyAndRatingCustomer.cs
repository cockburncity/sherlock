﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class PropertyAndRatingCustomer : Customer
    {
        private string _nucNameId { get; set; }
        private string _nucNameType { get; set; }
		private string _pensionType { get; set; }
		private string _govtResp { get; set; }

        public string NucNameId
        {
            get { return _nucNameId; }
            set { _nucNameId = value; }
        }

        public string NucNameType
        {
            get { return _nucNameType.Trim(); }
            set { _nucNameType = value; }
        }

		public string PensionType
		{
			get { return _pensionType; }
			set { _pensionType = value; }
		}

		public string GovtResp
		{
			get { return _govtResp; }
			set { _govtResp = value; }
		}

    }
}