﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerLookup.Models
{
    public class AssociatedEntity
    {

        private string _associationId { get; set; }
        private string _description { get; set; }
        private string _landId { get; set; }


        public string AssociationId
        {
            get { return _associationId; }
            set { _associationId = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string LandId
        {
            get { return _landId; }
            set { _landId = value; }
        }


    }
}