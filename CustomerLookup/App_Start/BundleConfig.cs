﻿using System.Web;
using System.Web.Optimization;

namespace CustomerLookup
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/customers.css"));

            bundles.Add(new Bundle("~/Scripts/base-frameworks.js")
                    .Include("~/Scripts/Angular/angular.min.js"
                    ,"~/Scripts/Angular/angular-route.js"
                    , "~/Scripts/Angular/jquery.min.js"
                    , "~/Scripts/Angular/clipboard.min.js"
                    ));

            bundles.Add(new Bundle("~/Scripts/spa.js")
                    .Include("~/Scripts/SPA/app.js",
                            "~/Scripts/SPA/services.js",
                            "~/Scripts/SPA/controllers.js"
                            ));

                

        }
    }
}
