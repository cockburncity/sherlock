﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CustomerLookup.Data;

namespace CustomerLookup.Controllers
{
    public class CustomerLookupControllerBackup : ApiController
    {

        COGNAREntities objApi = new COGNAREntities();

        //get customer by phone number
        [HttpGet]
        public IEnumerable<uspGetCustomerByPhone_Result> Phone(string systemKey, string lookupValue)
        {
            SystemKeys systemKeyValue = new SystemKeys();
            
            try
            {
                systemKeyValue = (SystemKeys)Enum.Parse(typeof(SystemKeys), systemKey);
            }
            catch (Exception e)
            {
                //this will just return a 404 response code in header
                var message = string.Format("No system key specified");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
 
            switch (systemKeyValue)
            {
                case SystemKeys.PropertyAndRating:
                    return objApi.uspGetCustomerByPhone(lookupValue).AsEnumerable();
                case SystemKeys.FinanceOne:
                    return objApi.uspGetCustomerByPhone(lookupValue).AsEnumerable();
                case SystemKeys.Amlib:
                    return objApi.uspGetCustomerByPhone(lookupValue).AsEnumerable();
                case SystemKeys.ClassLeisureWorld:
                    return objApi.uspGetCustomerByPhone(lookupValue).AsEnumerable();
                case SystemKeys.ClassFacilityBooking:
                    return objApi.uspGetCustomerByPhone(lookupValue).AsEnumerable();
                default:
                    //this will just return a 404 response code in header
                    var message = string.Format("Could not find system matching the specified key {0}", systemKey);
                    HttpError err = new HttpError(message);
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }  
        }

        //get library membership
        [HttpGet]
        public IEnumerable<uspGetLibraryMembership_Result> LibraryMembership(string LookupValue)
        {
            return objApi.uspGetLibraryMembership(LookupValue).AsEnumerable();
        }

        enum SystemKeys
        {
            PropertyAndRating,
            FinanceOne,
            Amlib,
            ClassLeisureWorld,
            ClassFacilityBooking
        };

    }
}
