﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CustomerLookup.Data;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using Dapper;
using System.Configuration;
using CustomerLookup.Models;

namespace CustomerLookup.Controllers
{
    public class CustomerLookupController : ApiController
    {

        //sql connection via Dapper
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());

        //get customer by phone number
        [HttpGet]
        public IEnumerable<Customer> Index(string lookupKey, string number = null, string givennames = null, string surname = null, string address = null, string email = null)
        {

            if (number != null && number.Contains("@"))
            {
                email = number;
                number = null;
            }
            if (number != null && number.Length < 8)
            {
                //this will just return a 404 response code in header
                var message = string.Format("number is not long enough.");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            if (givennames != null && givennames.Length < 2)
            {
                //this will just return a 404 response code in header
                var message = string.Format("givennames is not long enough.");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            if (surname != null && surname.Length < 2)
            {
                //this will just return a 404 response code in header
                var message = string.Format("surname is not long enough.");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            if (email != null && email.Length < 6)
            {
                //this will just return a 404 response code in header
                var message = string.Format("email is not long enough.");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            if (address != null && address.Length < 5)
            {
                //this will just return a 404 response code in header
                var message = string.Format("address is not long enough.");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            SystemKeys systemKeyValue = new SystemKeys();

            dynamic searchParameters = new ExpandoObject();

            if(number != null) {
                searchParameters.Number = number.Replace(" ", "%"); ;
            }
            if (givennames != null)
            {
                searchParameters.GivenNames = givennames.Replace(" ", "%");
            }
            if (surname != null)
            {
                searchParameters.Surname = surname.Replace(" ", "%");
            }
            if (address != null)
            {
                searchParameters.Address = address.Replace(" ", "%");
            }
            if (email != null)
            {
                searchParameters.Email = email.Replace(" ", "%");
            }

            try
            {
                systemKeyValue = (SystemKeys)Enum.Parse(typeof(SystemKeys), lookupKey);
            }
            catch (Exception e)
            {
                //this will just return a 404 response code in header
                var message = string.Format("No system key specified");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            switch (systemKeyValue)
            {
                case SystemKeys.PropertyAndRatingCustomer:
                    return con.Query<PropertyAndRatingCustomer>("PropertyAndRatingFindPeople", (object)searchParameters, commandType: CommandType.StoredProcedure);
                case SystemKeys.FinanceOne:
                    return con.Query<FinanceOneBusiness>("FinanceOneFindPeople", (object)searchParameters, commandType: CommandType.StoredProcedure);
                case SystemKeys.Amlib:
                    return con.Query<AmlibCustomer>("AmlibFindPeople", (object)searchParameters, commandType: CommandType.StoredProcedure);
                case SystemKeys.ClassLeisureWorld:
                    return con.Query<LeisureWorldCustomer>("ClassLeisureWorldFindPeople", (object)searchParameters, commandType: CommandType.StoredProcedure);
                case SystemKeys.ClassAdministration:
                    return con.Query<ClassAdministrationCustomer>("ClassAdministrationFindPeople", (object)searchParameters, commandType: CommandType.StoredProcedure);
                case SystemKeys.IntelliLeisure:
                    return con.Query<IntelliLeisureCustomer>("IntelliLeisureFindPeople", (object)searchParameters, commandType: CommandType.StoredProcedure);
                default:
                    //this will just return a 404 response code in header
                    var message = string.Format("Could not find system matching the specified key {0}", lookupKey);
                    HttpError err = new HttpError(message);
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
        }

        //get customer by phone number
        [HttpGet]
        public IEnumerable<Customer> Phone(string lookupKey, string lookupValue)
        {
            SystemKeys systemKeyValue = new SystemKeys();
            
            try
            {
                systemKeyValue = (SystemKeys)Enum.Parse(typeof(SystemKeys), lookupKey);
            }
            catch (Exception e)
            {
                //this will just return a 404 response code in header
                var message = string.Format("No system key specified");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
 
            switch (systemKeyValue)
            {
                case SystemKeys.PropertyAndRatingCustomer:
                    return con.Query<PropertyAndRatingCustomer>("PropertyAndRatingFindPeople",
                        new { number = lookupValue },
                            commandType: CommandType.StoredProcedure);
                case SystemKeys.FinanceOne:
                    return con.Query<FinanceOneBusiness>("FinanceOneFindPeople",
                       new { number = lookupValue },
                           commandType: CommandType.StoredProcedure);
                case SystemKeys.Amlib:
                    return con.Query<AmlibCustomer>("AmlibFindPeople",
                       new { number = lookupValue },
                           commandType: CommandType.StoredProcedure);
                case SystemKeys.ClassLeisureWorld:
                    return con.Query<LeisureWorldCustomer>("ClassLeisureWorldFindPeople",
                       new { number = lookupValue },
                           commandType: CommandType.StoredProcedure);
                case SystemKeys.ClassAdministration:
                    return con.Query<ClassAdministrationCustomer>("ClassAdministrationFindPeople",
                       new { number = lookupValue },
                           commandType: CommandType.StoredProcedure);
                case SystemKeys.IntelliLeisure:
                    return con.Query<IntelliLeisureCustomer>("IntelliLeisureFindPeople",
                       new { number = lookupValue },
                           commandType: CommandType.StoredProcedure);
                default:
                    //this will just return a 404 response code in header
                    var message = string.Format("Could not find system matching the specified key {0}", lookupKey);
                    HttpError err = new HttpError(message);
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }  
        }

        //get associated records
        [HttpGet]
        public IEnumerable<AssociatedEntity> Associated(string lookupKey, string lookupStatus, string lookupValue)
        {
            AssociationKeys associationKeyValue = new AssociationKeys();

            char lookupStatusChar = lookupStatus.FirstOrDefault(); //Change Current to C and Past to P

            try
            {
                associationKeyValue = (AssociationKeys)Enum.Parse(typeof(AssociationKeys), lookupKey);
            }
            catch (Exception e)
            {
                //this will just return a 404 response code in header
                var message = string.Format("No association key specified");
                HttpError err = new HttpError(message);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            switch (associationKeyValue)
            {
                case AssociationKeys.Animals:
                    return con.Query<AssociatedEntity>("PropertyAndRatingFindAnimal",
                        new
                        {
                            NucNameId = lookupValue,
                            Status = lookupStatusChar
                        },
                        commandType: CommandType.StoredProcedure);
                case AssociationKeys.Applications:
                    return con.Query<AssociatedEntity>("PropertyAndRatingFindApplications",
                        new
                        {
                            NucNameId = lookupValue,
                            Status = lookupStatusChar
                        },
                        commandType: CommandType.StoredProcedure);
                case AssociationKeys.Infringements:
                    return con.Query<AssociatedEntity>("PropertyAndRatingFindInfringements",
                       new
                       {
                           NucNameId = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);
                case AssociationKeys.Properties:
                    return con.Query<AssociatedEntity>("PropertyAndRatingFindProperties",
                       new
                       {
                           NucNameId = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);
                case AssociationKeys.Requests:
                    return con.Query<AssociatedEntity>("PropertyAndRatingFindRequests",
                       new
                       {
                           NucNameId = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);
                case AssociationKeys.Debtors:
                    return con.Query<AssociatedEntity>("PropertyAndRatingFindDebtors",
                       new
                       {
                           NucNameId = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);
                case AssociationKeys.Loans:
                    return con.Query<AssociatedEntity>("AmlibFindLoans",
                       new
                       {
                           BorrowerBarcodeNo = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);
                case AssociationKeys.LeisureWorldMemberships:


                    return con.Query<AssociatedEntity>("IntelliLeisureFindMemberships",
                       new
                       {
                           UserGuid = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);

                case AssociationKeys.LeisureWorldBookings:


                    return con.Query<AssociatedEntity>("IntelliLeisureFindBookings",
                       new
                       {
                           UserGuid = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);

                case AssociationKeys.LeisureWorldClasses:


                    return con.Query<AssociatedEntity>("IntelliLeisureFindClasses",
                       new
                       {
                           UserGuid = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);

                case AssociationKeys.LeisureWorldCourses:


                    return con.Query<AssociatedEntity>("IntelliLeisureFindCourses",
                       new
                       {
                           UserGuid = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);
                case AssociationKeys.LeisureWorldInvoices:


                    return con.Query<AssociatedEntity>("IntelliLeisureFindOutstandingInvoices",
                       new
                       {
                           UserGuid = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);

                case AssociationKeys.LeisureWorldGroups:


                    return con.Query<AssociatedEntity>("IntelliLeisureFindGroups",
                       new
                       {
                           UserGuid = lookupValue,
                           Status = lookupStatusChar
                       },
                       commandType: CommandType.StoredProcedure);


				case AssociationKeys.LeisureWorldAccounts:


					return con.Query<AssociatedEntity>("IntelliLeisureFindAccounts",
					   new
					   {
						   UserGuid = lookupValue,
						   Status = lookupStatusChar
					   },
					   commandType: CommandType.StoredProcedure);


                //
                default:
                    //this will just return a 404 response code in header
                    var message = string.Format("Could not find system matching the specified key {0}", lookupKey);
                    HttpError err = new HttpError(message);
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
        }

        enum SystemKeys
        {
            PropertyAndRatingCustomer,
            FinanceOne,
            Amlib,
            ClassLeisureWorld,
            ClassAdministration,
            IntelliLeisure
        };

        enum AssociationKeys
        {
            Animals,
            Applications,
            Infringements,
            Properties,
            Requests,
            Debtors,
            Loans,
            LeisureWorldMemberships,
            LeisureWorldBookings,
            LeisureWorldCourses,
            LeisureWorldClasses,
            LeisureWorldInvoices,
            LeisureWorldGroups,
			LeisureWorldAccounts
        };

    }
}
