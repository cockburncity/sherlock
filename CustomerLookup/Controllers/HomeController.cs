﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using CustomerLookup.Data;

namespace CustomerLookup.Controllers
{
    public class LookupController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
 
            return View();
        }

        public ActionResult Phone(string id)
        {
            ViewBag.Title = "Lookup Phone";

            string[] exceptions = new string[] { "01", "02", "03", "05", "06", "07", "08", "09" };

            if (id != null)
            {
               //check if
                if (id.StartsWith("0"))
                {
                    id = id.Substring(1); //strip the leading 0 from the phone number, which is added by the phone system
                }
                if (exceptions.Any(exception => id.StartsWith(exception)))
                {
                    id = id.Substring(2,id.Length-2); //strip the leading 0 from the phone number, which is added by the phone system
                }
                if (id.Length <= 4)
                {
                    //eg '3308', redirect to intranet phonebook if a staff member rings
                    Response.Redirect(string.Format("http://cognet/Staff/Organisation/Staff_directory?employeekeyword={0}", id));
                }
            }

            

            ViewBag.PhoneNumber = id;

            return View();
        }

        public ActionResult IntraMaps(string xmlurlin)
        {
            //this is a workaround for custom url protocol handler not being able to handle special characters (&, %) in intramaps query string
            var encodedxmlurlin = HttpUtility.UrlEncode(xmlurlin);
			var url = string.Format("http://roma/intramaps90/?project=Gosnells&module=Property&layer=Cadastre&xmlurlin={0}", encodedxmlurlin);

            Response.Redirect(url);
            return Content("Redirecting");
        }

        public ActionResult IntraMapsWaste(string xmlurlin)
        {
            //this is a workaround for custom url protocol handler not being able to handle special characters (&, %) in intramaps query string
            var encodedxmlurlin = HttpUtility.UrlEncode(xmlurlin);
			var url = string.Format("http://roma/intramaps90/?project=Gosnells&module=Customer%20Service&layer=cadastre&xmlurlin={0}", encodedxmlurlin);

            Response.Redirect(url);
            return Content("Redirecting");
        }

        public ActionResult IntramapsFireInspection(string xmlurlin)
        {
            //this is a workaround for custom url protocol handler not being able to handle special characters (&, %) in intramaps query string
            var encodedxmlurlin = HttpUtility.UrlEncode(xmlurlin);
			var url = string.Format("http://roma/intramaps90/?project=Gosnells&module=Firebreak&layer=Firebreak%20Inspection&xmlurlin={0}", encodedxmlurlin);

            Response.Redirect(url);
            return Content("Redirecting");
        }

        public ActionResult AmlibRenew(string barcodeNo,string systemCounter)
        {
            var request = (HttpWebRequest)WebRequest.Create("http://libcat.gosnells.wa.gov.au/amlibweb/webquery.dll");

            var postData = "v18=2E";
            postData += "&v20=2";
            postData += "&v22=2E";
            postData += "&v46=" + systemCounter;
            postData += "&v31=" + barcodeNo;
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.AllowAutoRedirect = true;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            var redirectUrl = response.ResponseUri.ToString();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            response.Close();
            Response.Redirect(redirectUrl);
            return Content("");
        }
    }
}
