Things to update
- database connection strings, eg CustomerLookup\Web.config
- CustomerLookup\Views\Lookup\Phone.cshtml: at the top, enter an email address where you want callback emails to be BCCd to (eg Customer Server Coordinator). I think our Coordinator created a rule in Outlook to move these emails into a folder. 
- CustomerLookup\Views\Lookup\Phone.cshtml: you'll noticed some custom protocols like cilink:// and ie:// and iessl://. You should hopefully already have cilink:// as that's provided by T1. ie:// and iessl:// are protocols we created and are distributed through group policy, and both of them open the link in a new ie window. We did this so that the links didn't open inside the same ie window embedded in MiContact Centre Solidus
